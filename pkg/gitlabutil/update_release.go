package gitlabutil

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"
	"github.com/xanzy/go-gitlab"
)

// UpdateReleaseOptions for gitlab release update api
// https://docs.gitlab.com/ee/api/tags.html#update-a-release
type UpdateReleaseOptions struct {
	ID          string `json:"id"`
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
}

// UpdateReleaseResponse for gitlab release update api
// https://docs.gitlab.com/ee/api/tags.html#update-a-release
type UpdateReleaseResponse struct {
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
}

// UpdateTagDescription updates release note
func UpdateTagDescription(client *gitlab.Client, project string, tagID string, description string) (*UpdateReleaseResponse, *gitlab.Response, error) {
	updateOptions := &UpdateReleaseOptions{
		ID:          project,
		TagName:     tagID,
		Description: description,
	}
	updateResp := UpdateReleaseResponse{}
	u := fmt.Sprintf("projects/%s/repository/tags/%s/release", url.QueryEscape(project), tagID)
	req, err := client.NewRequest("PUT", u, updateOptions, []gitlab.OptionFunc{})
	if err != nil {
		return nil, nil, errors.Wrap(err, "add-download make request")
	}

	resp, err := client.Do(req, &updateResp)
	if err != nil {
		return &updateResp, resp, errors.Wrap(err, "add-download execute request")
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return &updateResp, resp, errors.Errorf("unexpected status for release update %d", resp.StatusCode)
	}

	return &updateResp, resp, nil
}
